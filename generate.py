#!/usr/bin/env python

import sys, os, pickle, re
import time
#scriptPath = os.path.dirname(os.path.realpath(__file__))
#sys.path.append(scriptPath + os.sep + 'lib')

import itertools
import os.path
from tiddlywiki import TiddlyWiki

from difflib import SequenceMatcher

from nltk.tokenize import RegexpTokenizer
from nltk.util import ngrams

from multiprocessing import Pool

# import IPython


def makeIndex():

    import sys
    reload(sys)  
    sys.setdefaultencoding('utf8')

    sigs = []
    with open('significant_words.txt', 'r') as sig:
        for line in sig:
            if len(line.strip()) > 0:
                sigs.append(line.strip().lower())

    sig_dict = {s:[] for s in sigs}
    for d in deduped:
        lower = d.text.lower()
        for s in sig_dict:
            if s.lower() in lower:
                if re.search(r'\b' + s + r'\b', lower, re.I):
                    sig_dict[s].append(d)



    sig_counts = Counter({k:len(sig_dict[k]) for k in sig_dict})

def main(argv):

    writeToPickle("db_nodup_full.pickle")


def makeDirIfNeeded(path):
    try:
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise


def writeToPickle(pickleFilename):
    htmlfiles = [os.path.join(root, name)
                 for root, dirs, files in os.walk("Twine Spool")
                 for name in files
                 if name.endswith((".html", ".htm"))]

    allTWs = []
    for filename in htmlfiles:
        print(filename)
        if "Bell Park" in filename:
            continue
        allTWs.append(processTW(filename))

    # filename = "Twine Spool/Abnormals/index.html"
    # allTWs.append(processTW(filename))

    # filename = "Twine Spool/Noname/index.html"
    # allTWs.append(processTW(filename))

    # filename = "Twine Spool/where are you/index.html"
    # allTWs.append(processTW(filename))


    #manually did this!
    # tws = reduceTWSDuplicates(allTWs)

    tws = allTWs

    # print >> sys.stderr, "reduced TWS dups"

    filename = "Twine Spool/Bell Park, Youth Detective/index.html" #it's got some false dups 
    tws.append(processTW(filename))


    ts = []
    dedupedTs = []

    bigrams = {}
    trigrams = {}

    for tw in tws:
        for t in tw.tiddlers:
            tw.tiddlers[t].source = tw
            tw.tiddlers[t].update()
            ts.append(tw.tiddlers[t])


    print >> sys.stderr, "updated"

    ts = filter(onlyVanilla, ts)

    print >> sys.stderr, "filtered"

    # for tw in tws:
    #     dedupedTs.extend(reduceDuplicates(tw.tiddlers.itervalues()))

    p = Pool()
    dedupedTs = list(itertools.chain.from_iterable(map(reduceDuplicates, tws)))

    dedupedTs = filter(onlyVanilla, dedupedTs)

    print >> sys.stderr, "deduped"


    for t in dedupedTs:
        bigramify(t, bigrams, 2)

    bigrams = filterBigrams(bigrams, 2)

    print >> sys.stderr, "bigrammed"


    for t in dedupedTs:
        bigramify(t, trigrams, 3)

    trigrams = filterBigrams(trigrams, 3)

    print >> sys.stderr, "trigrammed"


    # random.shuffle(ts)
    allPassages = {}

    for t in ts:
        if (allPassages.has_key(t.title)):
            allPassages[t.title].append(t)
        else:
            allPassages[t.title] = [t,]

    print >> sys.stderr, "dictified"

    pickled = {}

    pickled['tws'] = tws
    pickled['ts'] = ts
    pickled['bigrams'] = bigrams
    pickled['trigrams'] = trigrams
    pickled['dedupedTs'] = dedupedTs
    pickled['allPassages']  = allPassages



    # import IPython
    # IPython.embed()
    # return

    print pickleFilename
    with open(pickleFilename, "wb") as f:
        pickle.dump(pickled, f)

    print "written"
    # import IPython
    # IPython.embed()


def filterBigrams(bigrams, n):
    filteredBigrams = {}

    for b in bigrams:
        if len(bigrams[b]) > 1: # and len(set(map(lambda t: t.source, bigrams[b]))) > 1 and len(set(map(lambda t: t.title.strip(), bigrams[b]))) > 2:
            filteredBigrams[b] = bigrams[b]

    bigrams = filteredBigrams

    removedBigrams = set(bigrams) - set(filteredBigrams)

    for b in removedBigrams:
        for t in removedBigrams[b]:
            try:
                if n == 2:
                    t.bigrams.remove(b)
                elif n == 3:
                    t.trigrams.remove(b)
            except KeyError:
                print >> sys.stderr, "key error filtering bigrams"
            except ValueError:
                print >> sys.stderr, "value error filtering bigrams"


    del(filteredBigrams)

    return bigrams

def onlyVanilla(t):
    
    if not t.isStoryPassage():
        return False
    

    try:
        if len(t.macros) > 0:
            return False
    except AttributeError:
        t.update()
        if len(t.macros) > 0:
            return False

    if len(t.images) > 0:
        return False
    
    # if len(t.links) > 0:
    #     return False
    
    if len(t.displays) > 0:
        return False
    
    if len(t.variableLinks) > 0:
        return False
    
    try:
        if t.source:
            pass
    except AttributeError:
        print >> sys.stderr, "FAIL we lost source attribution for ", t.title.encode('utf_8_sig')
        return False


    return True




def reduceDuplicates(tw):


    ts = tw.tiddlers.itervalues()

    print >> sys.stderr, os.getpid(), "deduping ", tw.filename, len(tw.tiddlers)

    if "a kiss" in tw.filename:
        return list(ts)

    ts = filter(lambda t: t.text.strip() != "", ts)

    uniqueTs = []

    starttime = time.time()

    seq = SequenceMatcher(lambda x: x in " \t\n\r.,", autojunk = False)
    for t in ts:
        seq.set_seq2(t.text)
        unique = True
        if len(t.text) > 2000:
            continue
        for t2 in uniqueTs:
            seq.set_seq1(t2.text)
            # print >> sys.stderr, "testing (after filter) ", tw, " and ", tw2, "len(common) is ", len(common)
            if seq.real_quick_ratio() > .95 and seq.ratio() > .95:
                unique = False
                # try:
                #     print >> sys.stderr, "not unique ", t.title.encode('utf_8_sig'), " and ", t2.title.encode('utf_8_sig'), " ratio:",seq.ratio()
                #     print >> sys.stderr, "------------------------------------------------"
                #     print >> sys.stderr,  t.text
                #     print >> sys.stderr, "------"
                #     print >> sys.stderr,  t2.text
                #     print >> sys.stderr, "------------------------------------------------"

                # except KeyError:
                #     print >> sys.stderr, "not unique no title found"
                break
        if unique:
            uniqueTs.append(t)

    print >> sys.stderr, os.getpid(), "deduping ", tw.filename, time.time() - starttime, len(uniqueTs), "(of", len(tw.tiddlers), ")"
    return uniqueTs


def bigramify(t, allBigrams, n):


    cleaned_text = re.sub(r'\[\[[^\]]*\]\]', '\n', t.text)
    cleaned_text = re.sub(r'\<[^\>]*\>', '\n', cleaned_text)
    fragments = cleaned_text.splitlines()
    bigrams = []

    t.text


    tokenizer = RegexpTokenizer('\s+', gaps=True)
    
    for fragment in fragments:
        tokens = tokenizer.tokenize(fragment)
        bigrams.extend(ngrams(tokens,n))
    
    bigrams = list(set(bigrams))

    if n == 2:
        t.bigrams = bigrams
    elif n == 3:
        t.trigrams = bigrams

    for bigram in bigrams:
        if bigram in allBigrams:
            allBigrams[bigram].append(t)
        else:
            allBigrams[bigram] = [t,]



def processTW(filename):
    tw = htmlToTW(filename)
    tw.filename = filename
    #filteredTiddlers = [tw.tiddlers[name] for name in tw.tiddlers if tw.tiddlers[name].isStoryPassage()]
    return tw


def reduceTWSDuplicates(tws):

    uniqueTws = []
    for tw in tws:
        ts = set(tw.tiddlers.iterkeys())
        unique = True
        for tw2 in uniqueTws:
            ts2 = set(tw2.tiddlers.iterkeys())
            common = ts & ts2
            # print >> sys.stderr, "testing ", tw, " and ", tw2, "len(common) is ", len(common)

            #lambda x: x in " \t\n\r.,"

            # for title in common:
            #     print >> sys.stderr, "------------------------------------------------"
            #     print >> sys.stderr,  tw.tiddlers[title].text
            #     print >> sys.stderr, "------"
            #     print >> sys.stderr,  tw2.tiddlers[title].text
            #     print >> sys.stderr, "===============" , SequenceMatcher(lambda x: x in " \t\n\r.,", tw.tiddlers[title].text, tw2.tiddlers[title].text).real_quick_ratio() 
            #     print >> sys.stderr, "------------------------------------------------"
            common = filter(lambda t: SequenceMatcher(lambda x: x in " \t\n\r.,", tw.tiddlers[t].text, tw2.tiddlers[t].text).real_quick_ratio() > .9, common) 

            # print >> sys.stderr, "testing (after filter) ", tw, " and ", tw2, "len(common) is ", len(common)

            if len(common) > 18:
                unique = False
                try:
                    print >> sys.stderr, "not unique ", tw.filename, " and ", tw2.filename, "len(common) is ", len(common)
                except KeyError:
                    print >> sys.stderr, "not unique no title found"
        if unique:
            uniqueTws.append(tw)
    return uniqueTws



def htmlToTW(filename):

    tw = TiddlyWiki()
    tw.addHtmlFromFilename(filename)
    return tw


if __name__ == '__main__':
    main(sys.argv[1:])


def countMacros(ts):
    macros = {}
    for t in ts:
        t.update()
        for m in t.macros:
            if m in macros:
                macros[m] += 1
            else:
                macros[m] = 1
    return macros
