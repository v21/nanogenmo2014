#!/usr/bin/env python
try:
   import cPickle as pickle
except:
   import pickle


import sys, os, random, re
#scriptPath = os.path.dirname(os.path.realpath(__file__))
#sys.path.append(scriptPath + os.sep + 'lib')

import os.path, traceback
from header import Header
from tiddlywiki import TiddlyWiki, Tiddler
from version import versionString

from difflib import SequenceMatcher

import itertools

import tweeregex

from tweelexer import TweeLexer

from collections import Counter


from nltk.tokenize import RegexpTokenizer
from nltk.util import ngrams
# import IPythonsub

AIMED_MAX_PASSAGE_COUNT = 100

class TweeApp(object):

    NAME = 'Twee'
    VERSION = versionString

    def __init__ (self, path):
        """Initializes the application."""
        self.builtinTargetsPath = path + os.sep + 'targets' + os.sep

    def displayError(self, activity, stacktrace = True):
        activity = activity.strip()
        print >> sys.stderr, 'Error while ' + activity
        if stacktrace:
            print >> sys.stderr, traceback.format_exc(5)
        else:
            exceptionValue = sys.exc_info()[1]
            if exceptionValue is not None:
                print >> sys.stderr, exceptionValue
            print >> sys.stderr, ''



def main (argv):


    import sys
    reload(sys)  
    sys.setdefaultencoding('utf8')

    print >> sys.stderr, "loading pickle"

    pickled = readFromPickle("db_full_w_obsessions.pickle")
    # tws = readFromPickle("db_dup.pickle")
    

    print >> sys.stderr, "loaded"

    tws = pickled['tws']
    ts = pickled['ts']
    bigrams = pickled['bigrams']
    trigrams = pickled['trigrams']
    dedupedTs = pickled['dedupedTs']
    allPassages = pickled['allPassages']
    sig_dict = pickled['sig_dict']




    print >> sys.stderr, "assigned from dicts"


    rootfilepath = "outputs/batch15/"

    makeDirIfNeeded(rootfilepath)

    for x in xrange(3, 30):
    # x = 1
        filepath = rootfilepath + os.sep + str(x) 
        gen(ts, tws, allPassages, bigrams, trigrams, dedupedTs, sig_dict, filepath)




def gen(ts, tws, allPassages, bigrams, trigrams, dedupedTs, sig_dict, filepath):


    print >> sys.stderr, "------------- GENERATING ", filepath

    sig_counts = Counter({k:len(sig_dict[k]) for k in sig_dict})


    obsessions = {tup[0]: sig_dict[tup[0]] for tup in random.sample(sig_counts.most_common(1000), 2)}

    print >> sys.stderr, "obsessions ", obsessions.keys()


    linksToFind = set()
    linksFound = set()

    usedPassages = []

    linksToFind.add(u'Start')

    while (len(linksToFind) > 0):
        linkToFind = random.sample(linksToFind, 1)[0] #progressing linearly feels like it'd make the graph weird, as we proceed depth first and limit things based on total node count
        linksToFind.remove(linkToFind)
        p = findPassage(linkToFind, allPassages, dedupedTs, usedPassages, bigrams, trigrams, linksToFind, linksFound, obsessions, sig_dict)
        ps = maybeManglePassage(p, trigrams, linksFound, linksToFind, obsessions, sig_dict)
        for p in ps:
            if (re.search(r'\[\[\|', p.text)) or not (re.search(r'\w+', p.text)): #stupid empty-link passages
                linksToFind.add(linkToFind)
            else:
                addPassage(p, allPassages, usedPassages, linksToFind, linksFound)


    tw = TiddlyWiki()
    usedPDict = {}

    filenames = u":: Twine Source Files Used"

    sources = set([p.source for p in usedPassages])
    for source in sources:
        try:
            filenames = filenames + u"\n" + source.tiddlers['StoryTitle'].text + u" by " + source.tiddlers['StoryAuthor'].text + u" (" + source.filename + u")"
        except (KeyError, UnicodeDecodeError):
            try:
                filenames = filenames + u"\n" + source.filename.encode('utf_8_sig')
            except (KeyError, UnicodeDecodeError):
                filenames = filenames + u"\n Missing source info, unicode error"


    attribTW = Tiddler(filenames)

    usedPassages.append(attribTW)

    for p in usedPassages:
        usedPDict[p.title] = p

    tw.tiddlers = usedPDict

    with open(filepath + ".tw", "w") as twoutp:
        twoutp.write(tw.toTwee().encode('UTF-8'))
    

    makeHtml(tw, filepath)


def addPassage(passage, allPassages, usedPassages, linksToFind, linksFound):
    print >> sys.stderr , "Adding passage", passage.toTwee().encode('utf_8_sig')

    if passage is None:
        return
    try :
        if passage.source:
            pass
    except AttributeError:
        print >> sys.stderr , "lost passage source", passage.title.encode('utf_8_sig')

    passage.update()
    usedPassages.append(passage)
    links = set(passage.links)
    links = links - linksFound
    linksToFind |= links
    linksFound.add(passage.title)
    linksToFind -= linksFound


def maybeManglePassage(passage, trigrams, linksFound, linksToFind, obsessions, sig_dict, previousMangled = None, previousMangledPassages = None):
    if previousMangled is None: previousMangled = [] 
    if previousMangledPassages is None: previousMangledPassages = [] 
    #should switch on a bigram?
    #let's find possible switching bigrams

    try:
        passage.random
    except AttributeError:
        passage.random = False
    
    #make existing links self-referential
    text = passage.text
    offset = 0
    for m in re.finditer(tweeregex.LINK_REGEX, passage.text):
        if m.group(2):
            g = 2
        else:
            g = 1
        try:
            if (len(linksFound) > AIMED_MAX_PASSAGE_COUNT * 0.8 and random.random < 0.8) or random.random() < 0.15:
                newLink = random.choice(list(linksFound | linksToFind))

                text = text[:m.start(g) + offset] + newLink + text[m.end(g) + offset:]
                print >> sys.stderr, "winding it down, linking to existing passage ", newLink, "replacing", m.group(g), "offset", offset

                offset -= len(m.group(g))
                offset += len(newLink)
        except IndexError:
            pass
        
        passage = cloneTiddler(passage)
        passage.text = text


    if (random.random() < 0.2) and len(linksFound) + len(linksToFind) < AIMED_MAX_PASSAGE_COUNT:
        for obsession in obsessions:    
            obsession_match = re.search(r'\b' + obsession + r'\b', text, re.I)
            if obsession_match:
                inLink = False
                for m in re.finditer(tweeregex.LINK_REGEX, text):
                    if (m.start(0) < obsession_match.start(0)) and (m.end(0) > obsession_match.end(0)):
                        inLink = True
                if not inLink:
                    if (random.random() < 0.5):
                        l = list(obsession)
                        random.shuffle(l)
                        link =''.join(l)
                    else:
                        link = obsession
                    new_link = "[[" + obsession_match.group(0) + '|' + link + ']]'
                    print >> sys.stderr, "making a link from obsession ", obsession, "to", new_link
                    text = text[:obsession_match.start(0)] + new_link + text[obsession_match.end(0):]

                    passage = cloneTiddler(passage)
                    passage.text = text
                    break



    if ((random.random() < 0.7 and passage.random) or (random.random() < 0.3 and not passage.random)) and len(previousMangled) < 4:
        try:
            possibleSwitchingBigrams = [b for b in passage.trigrams if b in trigrams.iterkeys()]
        except AttributeError:
            bigramify(passage, trigrams, 3)
            possibleSwitchingBigrams = [b for b in passage.trigrams if b in trigrams.iterkeys()]

        possibleSwitchingBigrams = filter(lambda bigram : bigram not in previousMangled, possibleSwitchingBigrams)

        result = ""


        if len(possibleSwitchingBigrams) > 0 :

            otherPassages = itertools.chain.from_iterable([trigrams[tri] for tri in possibleSwitchingBigrams])

            otherPassages = filter(lambda p : p.source.filename != passage.source.filename and p.title not in previousMangledPassages, otherPassages)

            if len(otherPassages) > 0:
                filteredOtherPassages = []

                obs = []

                for op in otherPassages:
                    for ob in obsessions.keys():
                        obsession_match = re.search(r'\b' + ob + r'\b', op.text, re.I)
                        if obsession_match:
                            filteredOtherPassages.append(op)

                if len(filteredOtherPassages) > 0:
                    otherPassage = random.choice(filteredOtherPassages)
                else:

                    for sig in sig_dict.keys():
                        if sig in passage.text.lower():
                            obs.append(sig)

                    for op in otherPassages:
                        for ob in obs:
                            obsession_match = re.search(r'\b' + ob + r'\b', op.text, re.I)
                            if obsession_match:
                                filteredOtherPassages.append(op)


                    if len(filteredOtherPassages) > 0:
                        otherPassage = random.choice(filteredOtherPassages)
                    else:
                        otherPassage = random.choice(otherPassages)


                try:
                    switchingBigram = random.choice(list(set(otherPassage.trigrams) & set(passage.trigrams)))
                except AttributeError:
                    bigramify(otherPassage, trigrams, 3)
                    switchingBigram = random.choice(list(set(otherPassage.trigrams) & set(passage.trigrams)))


                oldPassage = passage
                passage = cloneTiddler(passage)

                if not passage.random or random.random() < 0.7:
                    first = passage.text
                    second = otherPassage.text
                else:
                    first = otherPassage.text
                    second = passage.text

                print >> sys.stderr, "mangling",passage,"w trigrams - mangling on", switchingBigram, "with", otherPassage, ": obs", obs, "found", len(filteredOtherPassages)
                # print >> sys.stderr, "prev Mangled Passages", previousMangledPassages

                trigramRegex = re.escape(switchingBigram[0]) + r'\W+' + re.escape(switchingBigram[1]) + r'\W+' + re.escape(switchingBigram[2])

                thisPassageMatch = re.search(r'\A(.*)' + trigramRegex, first, re.DOTALL)

                if thisPassageMatch:
                    result += thisPassageMatch.group(1)
                else:
                    print >> sys.stderr, "can't find this passage match"

                result += " ".join(switchingBigram)

                otherPassageMatch = re.search(trigramRegex + r'(.*)\Z', second, re.DOTALL)
                if otherPassageMatch:
                    result += otherPassageMatch.group(1)
                else:
                    print >> sys.stderr, "can't find other passage match"


                passage.text = result
                passage.title = oldPassage.title
                previousMangled.append(switchingBigram)
                previousMangledPassages.append(otherPassage.title)
                return maybeManglePassage(passage, trigrams, linksFound, linksToFind, obsessions, sig_dict, previousMangled, previousMangledPassages)

    paras = passage.text.splitlines()
    paras = filter(lambda p : len(p) > 0, paras)

    if (random.random() < 0.5 and passage.random) or (random.random() < 0.1 and not passage.random):
        new_paras = []
        for para in paras:
            if re.search(tweeregex.LINK_REGEX, para):
                new_paras.append(para)

        sep = random.choice(["\n\n", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "\n", "|", "\n\n", "\n&bull;", "~", " or "])
        text = sep.join(new_paras)
        paras = passage.text.splitlines()
        paras = filter(lambda p : len(p) > 0, paras)

        passage = cloneTiddler(passage)
        passage.text = text

    if (len(paras) > 3 and random.random() < 0.5) or (len(paras) > 5 and random.random() < 0.9):

        oldPassage = passage
        passage = cloneTiddler(passage)

        trim_to = random.randint(1, len(paras) - 2)
        text = '\n\n'.join(paras[:trim_to])


        if (random.random < 0.3):
            match = re.search(r".*([^\n\r\[\|\]]*?\Z)", text)
        else:
            match = re.search(r".*\s(['\w]+\s['\w]+)(?:[^\[\|\]\w]*?\Z)", text)
        if not match:
            match = re.search(r".*\s(['\w]+)(?:[^\[\|\]\w]*?\Z)", text)
        
        if not match and re.search(tweeregex.LINK_REGEX, text):

            passage.text = text
            passage.title = oldPassage.title

            print >> sys.stderr, "truncating passage, already contains a link ", passage

        if match:

            text = text[:match.start(1)] + '[[' + match.group(1) + "|" + match.group(1) + ']]' + text[match.end(1):]
            newTitle = match.group(1)
            
            passage.text = text
            passage.title = oldPassage.title

            subseqentPassage = cloneTiddler(passage)
            subseqentPassage.text = '\n\n'.join(paras[trim_to:])
            subseqentPassage.title = newTitle

            print >> sys.stderr, "expanding passage to multiple ", passage, subseqentPassage


            outp = [passage,] 

            if subseqentPassage.title in linksFound:
                pass
            elif random.random() < 0.4:
                linksToFind.add(subseqentPassage.title)
            else:
                outp.extend(maybeManglePassage(subseqentPassage, trigrams, linksFound, linksToFind, obsessions, sig_dict, previousMangled, previousMangledPassages))


            return outp

    return [passage,]



#now using deduped
def findPassage(title, allPassages, ts, usedPassages, bigrams, trigrams, linksToFind, linksFound, obsessions, sig_dict):
    print >> sys.stderr, "finding passage ", title.encode('utf_8_sig')

    passage = random.choice(ts)
    if len(linksFound) > AIMED_MAX_PASSAGE_COUNT:
        print >> sys.stderr, "linksFound over limit", len(linksFound)
        selfRefCount = 0
        while (len(set(passage.links) - set(linksFound)) > 0) and selfRefCount < 30:
            passage = findArbitraryPassage(title, allPassages, ts, usedPassages, bigrams, trigrams, linksToFind, linksFound, obsessions, True)
            selfRefCount += 1
        while len(passage.links) > 0 or len(set(passage.links) - set(linksFound)) > 0: 
            passage = findArbitraryPassage(title, allPassages, ts, usedPassages, bigrams, trigrams, linksToFind, linksFound, obsessions, True)
        passage = cloneTiddler(passage)
        passage.title = title
        ts.append(passage)
        allPassages[title] = [passage,]           
        return passage


    if (allPassages.has_key(title)) and random.random() < 0.5:
        possiblePasages = allPassages[title]
        usedSources = [p.source for p in usedPassages]
        possiblePasages = [p for p in possiblePasages if p.source.filename not in map(lambda s: s.filename, usedSources)]    
        if len(possiblePasages) > 0 and random.random() < 0.95: #possible infinite loop with our empty passage detection up top
            passage = random.choice(possiblePasages)
            print >> sys.stderr, "selected one of passage ", title.encode('utf_8_sig'), "of", len(possiblePasages)
            passage.random = True
            return passage
            

    passage = findArbitraryPassage(title, allPassages, ts, usedPassages, bigrams, trigrams, linksToFind, linksFound, obsessions)
    if len(linksFound) + len(linksToFind) < 20:
        while len(passage.links) == 0 or len(set(passage.links) - set(linksFound)) == 0:
            passage = findArbitraryPassage(title, allPassages, ts, usedPassages, bigrams, trigrams, linksToFind, linksFound, obsessions)

    passage = cloneTiddler(passage)
    passage.title = title
    ts.append(passage)
    allPassages[title] = [passage,]
    return passage




def cloneTiddler(passage):

    try:
        rand = passage.random
    except AttributeError:
        rand = False

    source = passage.source
    
    passage = Tiddler(passage.toTwee())
    passage.source = source
    passage.update()
    passage.random = rand
    #bigramify(passage, {},3) #fuck it, no point updating bigrams now
    return passage

def wasInObsessions(title, usedPassages, obsessions):

    linking_passages = [p for p in usedPassages if title in p.links]
    for obsession in obsessions:
        if len([p for p in linking_passages if p in obsessions[obsession]]) > 0:
            return obsession


    return None


def findArbitraryPassage(title, allPassages, ts, usedPassages, bigrams, trigrams, linksToFind, linksFound, obsessions, overLimit = False):
    
    pos_obsession = wasInObsessions(title, usedPassages, obsessions)
    if (pos_obsession and random.random() < 0.8) or random.random() < 0.7:
        if not pos_obsession:
            pos_obsession = random.choice(obsessions.keys())
        used_titles = [p.title for p in usedPassages]
        usedSources = [p.source for p in usedPassages]
        passages = filter(lambda p: p.title not in used_titles and (random.random < 0.5 or p.source not in usedSources), obsessions[pos_obsession])

        print >> sys.stderr, "filtered obsessions", len(obsessions[pos_obsession]), "to", len(passages)
        if len(passages) > 0:
            passage = random.choice(passages)
            if not (overLimit and (len(passage.links) > 0 or len(set(passage.links) - set(linksFound)) > 0)):
                print >> sys.stderr, "adding passage ", passage.title.encode('utf_8_sig'), " from obsession", pos_obsession
                return passage
            else:
                print >> sys.stderr, "OVER LIMIT not adding passage ", passage.title.encode('utf_8_sig'), " from obsession", pos_obsession

    #prefer repeating twines
    if len(linksFound) > 15 and random.random() < 0.3:
        previousTwinesFound = list()
        for passage in usedPassages:
            try:
                if not passage.source in previousTwinesFound:
                    previousTwinesFound.append(passage.source)
            except AttributeError:
                print >> sys.stderr, "FAILING to find source twine for ", passage.title.encode('utf_8_sig'), " from previous twine found "

        chosenSource = random.choice(previousTwinesFound)
        allPassagesFromSourceFile = filter(lambda t : t.source == chosenSource, ts)
        passage = random.choice(allPassagesFromSourceFile)
        if not (overLimit and (len(passage.links) > 0 or len(set(passage.links) - set(linksFound)) > 0)):
            print >> sys.stderr, "adding passage ", passage.title.encode('utf_8_sig'), " from previous twine found ", chosenSource
            return passage


    tris = []

    linking_passages = [p for p in usedPassages if title in p.links]
    for passage in linking_passages:
        try:
            tris.extend(passage.trigrams)
        except (AttributeError, KeyError):
            try:
                bigramify(passage, trigrams, 3)
                tris.extend(passage.trigrams)
            except:
                pass

    tris = filter(lambda t: t in trigrams, tris)
    p_tris = list(itertools.chain.from_iterable([trigrams[tri] for tri in tris]))
    usedSources = [p.source for p in usedPassages]
    possible_passages = [p for p in p_tris if len(set(tris) & set(p.trigrams)) < 10]
    print >> sys.stderr, "looking for trigram match with ", tris, " found of", len(p_tris), "possible", len(possible_passages)
    if len(possible_passages) > 0:
        passage = random.choice(possible_passages)
        if not (overLimit and (len(passage.links) > 0 or len(set(passage.links) - set(linksFound)) > 0)):
            print >> sys.stderr, "adding passage ", passage, " based on trigram match with one of ", linking_passages, " from", tris
            return passage
        else:
            print >> sys.stderr, "OVER LIMIT - not adding passage ", passage, " based on trigram match with one of ", linking_passages, " from", tris


    # p_tris = []
    # for passage in usedPassages:
    #     if title in passage.links:
    #         passage_matched = passage
    #         try:
    #             for bi in passage.bigrams:
    #                 p_tris.extend(bigrams[bi])
    #         except (AttributeError, KeyError):
    #             try:
    #                 bigramify(passage, bigrams, 2)
    #                 for bi in passage.bigrams:
    #                     p_tris.extend(bigrams[bi])
    #             except:
    #                 pass
    # usedSources = [p.source for p in usedPassages]
    # possible_passages = [p for p in p_tris if len(set(p_tris.bigrams) & set(p.bigrams)) < 10]
    # print >> sys.stderr, "looking for bigram match with ", passage.bigrams, " found of", len(p_tris), "possible", possible_passages
    # if len(possible_passages) > 0:
    #     passage = random.choice(possible_passages)
    #     if not (overLimit and len(passage.links) > 0 or len(set(passage.links) - set(linksFound)) > 0):
    #         print >> sys.stderr, "adding passage ", passage, " based on bigrams match with", passage_matched, " from", set(passage_matched.bigrams) & set(passage.bigrams)
    #         return passage




    passage = random.choice(ts)
    if (len(linksFound) < 30):
        while len(passage.links) == 0:
            passage = random.choice(ts)
    elif (len(linksFound) > AIMED_MAX_PASSAGE_COUNT and random.random() > 0.95):
        while len(passage.links) > 0 or len(set(passage.links) & set(linksFound)) > 0:
            passage = random.choice(ts)

    passage.random = True
    print >> sys.stderr, "found random passage", passage.title.encode('utf_8_sig')
    return passage




def bigramify(t, allBigrams, n):


    cleaned_text = re.sub(r'\[\[[^\]]*\]\]', '\n', t.text)
    cleaned_text = re.sub(r'\<[^\>]*\>', '\n', cleaned_text)
    fragments = cleaned_text.splitlines()
    bigrams = []

    t.text


    tokenizer = RegexpTokenizer('\s+', gaps=True)
    
    for fragment in fragments:
        tokens = tokenizer.tokenize(fragment)
        bigrams.extend(ngrams(tokens,n))
    
    bigrams = list(set(bigrams))

    if n == 2:
        t.bigrams = bigrams
    elif n == 3:
        t.trigrams = bigrams

    for bigram in bigrams:
        if bigram in allBigrams:
            allBigrams[bigram].append(t)



def reduceDuplicates(ts):

    ts = filter(lambda t: t.text.strip() != "", ts)

    uniqueTs = []


    seq = SequenceMatcher(lambda x: x in " \t\n\r.,", autojunk = False)
    for t in ts:
        seq.set_seq2(t.text)
        unique = True
        if len(t.text) > 2000:
            continue
        for t2 in uniqueTs:
            seq.set_seq1(t2.text)
            # print >> sys.stderr, "testing (after filter) ", tw, " and ", tw2, "len(common) is ", len(common)
            if seq.real_quick_ratio() > .95 and seq.ratio() > .95:
                unique = False
                # try:
                #     print >> sys.stderr, "not unique ", t.title.encode('utf_8_sig'), " and ", t2.title.encode('utf_8_sig'), " ratio:",seq.ratio()
                #     print >> sys.stderr, "------------------------------------------------"
                #     print >> sys.stderr,  t.text
                #     print >> sys.stderr, "------"
                #     print >> sys.stderr,  t2.text
                #     print >> sys.stderr, "------------------------------------------------"

                # except KeyError:
                #     print >> sys.stderr, "not unique no title found"
                break
        if unique:
            uniqueTs.append(t)

    return uniqueTs


def makeHtml(tw, filepath):

    # defaults

    target = 'sugarcane'

    # read command line switches

    scriptPath = os.path.dirname(os.path.realpath(__file__))
    app = TweeApp(scriptPath)

    header = Header.factory(target, app.builtinTargetsPath + target + os.sep, app.builtinTargetsPath)

    # generate output

    output = tw.toHtml(app, header)
    
    with open(filepath + ".html", "w") as outp:
        outp.write(output.encode('utf_8_sig'))


    # if output is None:
    #     print >> sys.stderr, 'twee: no output'
    # elif sys.stdout.isatty():
    #     print output
    # else:
    #     print output.encode('utf_8_sig')

def makeDirIfNeeded(path):
    try: 
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise

def readFromPickle(filename):

    with open(filename, "rb") as f:
        ts = pickle.load(f)

    return ts


def countMacros(ts):
    macros = {}
    for t in ts:
        t.update()
        for m in t.macros:
            if macros.has_key(m):
                macros[m] += 1
            else:
                macros[m] = 1
    return macros


if __name__ == '__main__':
    main(sys.argv[1:])
